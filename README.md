
## Required for setup - docker-compose


### Clone project

git clone --recursive https://gitlab.com/stabone/sun_proj.git


### Env configuration

In root directory rename .env.example to .env
In api directory rename .env.example to .env

DB configuration could be changed in docker-composer.yml file, pg_db section.
If db env. variables ar changed then .env file in api directory should be updated.


### Run project

In root directory execute *docker-compose up*


### Run migrations

This should be done after *docker-compose up*

docker exec -it container_id php artisan migrate

If containers isn't changed then container_id should be replaced with *php*


### Run tests

Tests could be run using *docker exec -it php php artisan test*


### Documentation

Documentation could be found by going to /api/documentation uri. If configuration wasn't changed then http://localhost:8000/api/documentation will be needed url.


### Ports

There should be few open ports 8000 (localhost:8000) for api and 5432 for postgres db
